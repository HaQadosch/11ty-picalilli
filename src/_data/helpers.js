module.exports = {
  /**
   * Returns back some attributes based on whether
   * the link is active or
   * a parent of an active item
   *
   * @param { String } itemURL the link in question
   * @param { String } pageURL the page context
   * @returns { String } the attributes or empty
   */
  getLinkActiveState (itemURL, pageURL) {

    let response = ''

    if (itemURL === pageURL) {
      response = 'aria-current="page"'
    }

    if (itemURL.length > 1 && pageURL.startsWith(itemURL)) {
      response += ' data-state="active"'
    }

    return response
  }
}
